angular.module('fsqrApp')
  .service('fsqrAPIservice', function ($resource, appConfig) {
    // service which calls resource with configuration data from appConfig

    // resource: A factory which creates a resource object that lets you interact with RESTful server-side data sources.
    // we need to pass three arguments into it:
    // (1) url (Foursquare RESTapi),
    // (2) default parameters,
    // (3) ngResource Actions object with standard configuration options (e.g. transformResponse)

    var url = appConfig.api.baseURL + '/venues/search';

    return $resource(url, {
      client_id: appConfig.api.clientId,
      client_secret: appConfig.api.clientSecret,
      v: appConfig.api.version,
      callback: 'JSON_CALLBACK'
    },
    {
      // here we define Actions object with custom properties names
      myFindPlaces: {                       // {string} – The name of action. This name becomes the name of the method on your resource object.
        // use standard configuration options of ngResource
        method: 'JSONP',                    // {string} – Case insensitive HTTP method (e.g. GET, POST, PUT, DELETE, JSONP, etc).
        isArray: true,                      // {boolean=} – If true then the returned object for this action is an array, see returns section.
        transformResponse: function(data) { // The transform function takes the http response body and headers and returns its transformed (typically deserialized) version. By default, transformResponse will contain one function that checks if the response looks like a JSON string and deserializes it using angular.fromJson.
          console.log(data);
          // pick a small part of the "data" response
          return data.response.venues;
        }
      }
    });
  });

  /* read it like this:
  --------------------------------
    prepared with module $resource,
    our "fsqrAPIservice" will request URL
    with "ACCESS CONFIG OPTS"
    and has an action MYFINDPLACES
    which is JSONP method
    returning ARRAY and
    return only VENUES property
  --------------------------------
  */

  /* request URL looks like this:
  --------------------------------
    https://api.foursquare.com/v2/venues/search
      ?callback=angular.callbacks._1
      &client_id=EM1CO2O43IH3KCW0UVJXBDBYIOO5IFD1ALE55THJROGV4RIQ
      &client_secret=HWP0B2EQSH4EH1CBEA52BAPXLMEOCTFYDTZVZ3KQZGHFVEYQ
      &near=Kharkiv
      &query=coffee
      &v=20130815
  --------------------------------
  P.S:
    angular.callbacks._1 --- representation of JSONP
  */