
angular.module('fsqrApp')
  .controller('MainCtrl', function ($scope, fsqrAPIservice) {
    // this is main controller linked to DOM element
    // talks to services (e.g. fsqrAPIservice)
    // outputs data into view through $scope

    // define defaults for models
    $scope.city = "Kharkiv";
    $scope.category = "coffee";

    // define function for search click
    $scope.search = function () {
      // (1) take parameters from inputs in the View
      // (2) request data
      // (3) save result into $scope.places (output in view as ng-repeat)
      
      $scope.places = fsqrAPIservice.myFindPlaces({ // (3), (2)
        near: $scope.city,      // (1)
        query: $scope.category  // (1)
      }); // {city1: {name: 'Foo'}, city2: {name: "Qux"}};

    };
  });